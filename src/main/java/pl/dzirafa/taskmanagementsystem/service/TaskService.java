package pl.dzirafa.taskmanagementsystem.service;

import pl.dzirafa.taskmanagementsystem.model.Task;
import pl.dzirafa.taskmanagementsystem.model.TaskData;

import java.util.List;
import java.util.Optional;

/**
 * Created by dzirafa on 19.12.2017
 */
public interface TaskService {
    Optional<Task> getTaskWithId(Long id);

    boolean taskExists(Long id);

    boolean addTask(Task newTask, TaskData newTaskData);

    boolean editTask (Task taskToEdit, TaskData taskDataToEdit);

    boolean deleteTask(Task taskToDelete, TaskData taskDataToDelete);

    List<Task> getAllTasks();

    List<Task> getTasksWithPerformerId(Long performerId);

    List<Task> getTasksWithPrincipalId(Long principalId);

    List<Task> getDoneTasksWithPerformerId(Long performerId);

    List<Task> getUnfinishedTasksWithPerformerId(Long performerId);

    void setTaskIsDone(Task taskNotDone);

    void setTaskIsUnfinished(Task taskDone);

    Integer countUnfinishedTask(Long id);


}
