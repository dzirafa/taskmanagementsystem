package pl.dzirafa.taskmanagementsystem.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dzirafa.taskmanagementsystem.dao.TaskDao;
import pl.dzirafa.taskmanagementsystem.model.Task;
import pl.dzirafa.taskmanagementsystem.model.TaskData;

import java.util.List;
import java.util.Optional;

/**
 * Created by dzirafa on 03.01.2018
 */
@Service(value = "taskService")
@Transactional
public class TaskServiceImpl implements TaskService {

    @Autowired
    TaskDao taskDao;

    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);

    @Override
    public Optional<Task> getTaskWithId(Long id) {
        LOG.debug("Getting the task of id: " + id);
        return taskDao.findByTaskId(id);
    }

    @Override
    public boolean taskExists(Long id) {
        LOG.debug("Checking if task " + id + " exists");
        return taskDao.taskExist(id);
    }

    @Override
    public boolean addTask(Task newTask, TaskData newTaskData) {
        LOG.debug("Register new task " + newTask);
        taskDao.addTask(newTask,newTaskData);
        return true;
    }

    @Override
    public boolean editTask(Task taskToEdit, TaskData taskDataToEdit) {
        taskDao.editTask(taskToEdit,taskDataToEdit);
        return true;
    }

    @Override
    public boolean deleteTask(Task taskToDelete, TaskData taskDataToDelete) {
        taskDao.deleteTask(taskToDelete,taskDataToDelete);
        return true;
    }

    @Override
    public List<Task> getAllTasks() {
        LOG.debug("Getting the list of all tasks");
        return taskDao.getAllTask();
    }

    @Override
    public List<Task> getTasksWithPerformerId(Long performerId) {
        LOG.debug("Getting the list of all tasks of user: " + performerId);
        return taskDao.findByPerformerId(performerId);
    }

    @Override
    public List<Task> getTasksWithPrincipalId(Long principalId) {
        LOG.debug("Getting the list of all tasks, where the user: " + principalId + " is a principal");
        return taskDao.findByPrincipalId(principalId);
    }

    @Override
    public List<Task> getDoneTasksWithPerformerId(Long performerId) {
        LOG.debug("Getting the list of all done tasks of user: " + performerId);
        return taskDao.findDoneByPerformerId(performerId);
    }

    @Override
    public List<Task> getUnfinishedTasksWithPerformerId(Long performerId) {
        LOG.debug("Getting the list of all unfinished tasks of user: " + performerId);
        return taskDao.findUnfinishedByPerformerId(performerId);
    }

    @Override
    public void setTaskIsDone(Task taskNotDone) {
        LOG.debug("Setting the task " + taskNotDone + " to done");
        taskDao.setTaskIsDone(taskNotDone);
    }

    @Override
    public void setTaskIsUnfinished(Task taskDone) {
        LOG.debug("Setting the task " + taskDone + " to not finished");
        taskDao.setTaskIsUnfinished(taskDone);
    }

    @Override
    public Integer countUnfinishedTask(Long id) {
        LOG.debug("Getting quantity of all unfinished tasks of user: " + id);
        return taskDao.countUnfinishedTask(id);
    }


}
