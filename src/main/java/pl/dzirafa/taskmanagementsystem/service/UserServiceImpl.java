package pl.dzirafa.taskmanagementsystem.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dzirafa.taskmanagementsystem.dao.UserDao;
import pl.dzirafa.taskmanagementsystem.model.User;
import pl.dzirafa.taskmanagementsystem.model.UserData;

import java.util.List;
import java.util.Optional;

/**
 * Created by dzirafa on 18.12.2017
 */

@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);

    @Override
    public Optional<User> getUserById(Long id) {
        LOG.debug("Getting the user of id: " + id);
        return userDao.findById(id);
    }

    @Override
    public Optional<User> getUserByLogin(String login) {
        LOG.debug("Getting the user of login: " + login);
        return userDao.findByLogin(login);
    }

    @Override
    public boolean userExist(String login) {
        LOG.debug("Checking if user " + login + " exists");
        return userDao.userExist(login);
    }

    @Override
    public List<User> getAllUsers() {
        LOG.debug("Getting the list of all users");
        return userDao.getAllUsers();
    }

    @Override
    public boolean addUser(User userToRegister, UserData userData) {
        LOG.debug("Register user " + userToRegister);
        userDao.addUser(userToRegister, userData);
        return true;
    }

    @Override
    public boolean editUser(User userToEdit, UserData userDataToEdit) {
        userDao.editUser(userToEdit,userDataToEdit);
        return true;
    }

    @Override
    public boolean deleteUser(User userToDelete, UserData userDataToDelete) {
        userDao.deleteUser(userToDelete, userDataToDelete);
        return true;
    }

    @Override
    public boolean userLogin(String login, String password) {
        Optional<User> loggingInUser = userDao.findByLogin(login);
        if (loggingInUser.isPresent()) {
            User user = loggingInUser.get();
            if(user.getPassword().equals(password)){
                return true;
            }else{
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public boolean confirmPassword(User user, String password) {
        return user.getPassword().equals(password);
    }

}
