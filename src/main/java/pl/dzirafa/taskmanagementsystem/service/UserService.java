package pl.dzirafa.taskmanagementsystem.service;

import pl.dzirafa.taskmanagementsystem.model.User;
import pl.dzirafa.taskmanagementsystem.model.UserData;

import java.util.List;
import java.util.Optional;

/**
 * Created by dzirafa on 18.12.2017
 */
public interface UserService {

    Optional<User> getUserById(Long id);

    Optional<User> getUserByLogin(String login);

    boolean userExist (String login);

    List<User> getAllUsers();

    boolean addUser(User userToRegister, UserData userData);

    boolean editUser (User userToEdit, UserData userDataToEdit);

    boolean deleteUser(User userToDelete, UserData userDataToDelete);

    boolean userLogin(String login, String password);

    boolean confirmPassword(User user, String password);

}
