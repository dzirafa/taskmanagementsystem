package pl.dzirafa.taskmanagementsystem.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import pl.dzirafa.taskmanagementsystem.model.User;
import pl.dzirafa.taskmanagementsystem.model.UserData;

import java.util.List;
import java.util.Optional;

/**
 * Created by dzirafa on 18.12.2017
 */
@Repository(value = "userDao")
public class UserDaoImpl extends AbstractDao implements UserDao {

    @Override
    public Optional<User> findById(Long id) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("userId", id));//
        return Optional.ofNullable((User) criteria.uniqueResult());
    }

    @Override
    public Optional<User> findByLogin(String login) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("login", login).ignoreCase());
        return Optional.ofNullable((User) criteria.uniqueResult());
    }

    @Override
    public boolean userExist(String login) {
        long resultCount = (long) getSession().createCriteria(User.class)
                .add(Restrictions.eq("login", login).ignoreCase())
                .setProjection(Projections.rowCount()).uniqueResult();
        return resultCount > 0;
    }

    @Override
    public List<User> getAllUsers() {
        return getSession().createCriteria(User.class).list();
    }

    @Override
    public void addUser(User newUser, UserData newUserData) {
        persist(newUserData);
        newUser.setUserData(newUserData);
        persist(newUser);
    }

    @Override
    public void editUser(User userToEdit, UserData userDataToEdit) {
        update(userDataToEdit);
        userToEdit.setUserData(userDataToEdit);
        update(userToEdit);
    }

    @Override
    public void deleteUser(User userToDelete, UserData newUserData) {
        delete(userToDelete);
        delete(newUserData);
    }


}
