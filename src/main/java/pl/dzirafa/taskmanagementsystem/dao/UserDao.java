package pl.dzirafa.taskmanagementsystem.dao;

import pl.dzirafa.taskmanagementsystem.model.User;
import pl.dzirafa.taskmanagementsystem.model.UserData;

import java.util.List;
import java.util.Optional;

/**
 * Created by dzirafa on 18.12.2017
 */
public interface UserDao {

    Optional<User> findById(Long id);

    Optional<User> findByLogin(String login);

    boolean userExist(String login);

    List<User> getAllUsers();

    void addUser(User newUser, UserData newUserData);

    void editUser (User userToEdit, UserData newUserData);

    void deleteUser (User userToDelete, UserData newUserData);

}
