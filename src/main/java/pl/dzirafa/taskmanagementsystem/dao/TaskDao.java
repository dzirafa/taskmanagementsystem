package pl.dzirafa.taskmanagementsystem.dao;

import pl.dzirafa.taskmanagementsystem.model.Task;
import pl.dzirafa.taskmanagementsystem.model.TaskData;

import java.util.List;
import java.util.Optional;

/**
 * Created by dzirafa on 19.12.2017
 */
public interface TaskDao {
    Optional<Task> findByTaskId(Long id);

    boolean taskExist(Long id);

    void addTask(Task newTask, TaskData newTaskData);

    void editTask(Task taskToEdit, TaskData taskDataToEdit);

    void deleteTask(Task taskToDelete, TaskData taskDataToDelete);

    List<Task> getAllTask();

    List<Task> findByPerformerId(Long id);

    List<Task> findByPrincipalId(Long id);

    List<Task> findDoneByPerformerId(Long id);

    List<Task> findUnfinishedByPerformerId(Long id);

    void setTaskIsDone(Task task);

    void setTaskIsUnfinished(Task finishedTask);

    Integer countUnfinishedTask(Long id);
}
