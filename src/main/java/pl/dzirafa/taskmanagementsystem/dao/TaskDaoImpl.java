package pl.dzirafa.taskmanagementsystem.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import pl.dzirafa.taskmanagementsystem.model.Task;
import pl.dzirafa.taskmanagementsystem.model.TaskData;

import java.util.List;
import java.util.Optional;

/**
 * Created by dzirafa on 20.12.2017
 */
@Repository(value = "taskDao")
public class TaskDaoImpl extends AbstractDao implements TaskDao {

    @Override
    public Optional<Task> findByTaskId(Long id) {
        Criteria criteria = getSession().createCriteria(Task.class);
        criteria.add(Restrictions.eq("taskId",id));
        return Optional.ofNullable((Task) criteria.uniqueResult());
    }

    @Override
    public boolean taskExist(Long id) {
        if(id == null){
            return false;
        }
        long resultCount = (long) getSession().createCriteria(Task.class)
                .add(Restrictions.eq("taskId", id).ignoreCase())
                .setProjection(Projections.rowCount()).uniqueResult();
        return resultCount > 0;
    }

    @Override
    public void addTask(Task newTask, TaskData newTaskData) {
        persist(newTaskData);
        newTask.setTaskData(newTaskData);
        persist(newTask);
    }

    @Override
    public void editTask(Task taskToEdit, TaskData taskDataToEdit) {
        update(taskDataToEdit);
        taskToEdit.setTaskData(taskDataToEdit);
        update(taskToEdit);
    }

    @Override
    public void deleteTask(Task taskToDelete, TaskData taskDataToDelete) {
        delete(taskToDelete);
        delete(taskDataToDelete);
    }

    @Override
    public List<Task> getAllTask() {
        return getSession().createCriteria(Task.class).list();
    }

    @Override
    public List<Task> findByPerformerId(Long id) {
        return getSession().createCriteria(Task.class)
                .add(Restrictions.eq("performerId", id)).list();
    }

    @Override
    public List<Task> findByPrincipalId(Long id) {
        return getSession().createCriteria(Task.class)
                .add(Restrictions.eq("principalId", id)).list();
    }

    @Override
    public List<Task> findDoneByPerformerId(Long id) {
        return getSession().createCriteria(Task.class)
                .add(Restrictions.eq("performerId",id)).
                        add(Restrictions.eq("isDone",true)).list();
    }

    @Override
    public List<Task> findUnfinishedByPerformerId(Long id) {
        return getSession().createCriteria(Task.class)
                .add(Restrictions.eq("performerId",id)).
                        add(Restrictions.eq("isDone",false)).list();
    }

    @Override
    public void setTaskIsDone(Task task) {
        task.setDone(true);
        getSession().update(task);
    }

    @Override
    public void setTaskIsUnfinished(Task finishedTask) {
        finishedTask.setDone(false);
        getSession().update(finishedTask);
    }

    @Override
    public Integer countUnfinishedTask(Long id) {
        return getSession().createCriteria(Task.class)
                .add(Restrictions.eq("performerId",id)).
                        add(Restrictions.eq("isDone",false)).list().size();
    }
}
