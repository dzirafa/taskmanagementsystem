package pl.dzirafa.taskmanagementsystem.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Observable;

/**
 * Created by dzirafa on 18.12.2017
 */
public abstract class AbstractDao {
    @Autowired
    private SessionFactory sessionFactory;

    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }

    public void persist (Object entity){
        getSession().persist(entity);
    }

    public void delete (Object entity){
        getSession().delete(entity);
    }

    public void update (Object entity) {
        getSession().update(entity);
    }

    protected SessionFactory getSessionFactory(){
        return sessionFactory;
    }
}
