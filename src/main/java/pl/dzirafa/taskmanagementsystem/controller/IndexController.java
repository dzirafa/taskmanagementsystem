package pl.dzirafa.taskmanagementsystem.controller;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.dzirafa.taskmanagementsystem.model.AuthorizationData;
import pl.dzirafa.taskmanagementsystem.model.User;
import pl.dzirafa.taskmanagementsystem.service.UserService;

import java.util.Optional;

/**
 * Created by dzirafa on 20.12.2017
 */
@Controller
@RequestMapping(value = "/")
public class IndexController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/getUserList")
    public String getUserList() {
        return "/admin/UserList";
    }

    @RequestMapping(value = "/getTaskList")
    public String getTaskList() {
        return "/admin/TaskList";
    }

    @RequestMapping(value = "/userProfile/{id}")
    public String getUserProfile(ModelMap model, @PathVariable Long id) {
        model.addAttribute("user_id", id);
        return "/user/UserProfile";
    }

    @RequestMapping(value = "/userProfileNotEditable/{id}")
    public String getUserProfileNotEditable(ModelMap model, @PathVariable Long id) {
        model.addAttribute("user_id", id);
        return "/user/UserProfileNotEditable";
    }

    @RequestMapping(value = "/taskProfile/{id}")
    public String getTaskProfile(ModelMap model, @PathVariable Long id) {
        model.addAttribute("taskId", id);
        return "/task/TaskProfile";
    }

    @RequestMapping(value = "/taskProfileNotEditable/{id}")
    public String getTaskProfileNotEditable(ModelMap model, @PathVariable Long id) {
        model.addAttribute("taskId", id);
        return "/task/TaskProfileNotEditable";
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.GET)
    public String addUser() {
        return "/user/AddUser";
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public String addUserPost() {
        return "/user/AddUser";
    }

    @RequestMapping(value = "/addTask", method = RequestMethod.GET)
    public String addTask() {
        return "/admin/AddTask";
}

    @RequestMapping(value = "/addTask", method = RequestMethod.POST)
    public String addTaskPost() {
        return "/admin/AddTask";
    }

    @RequestMapping(value = "/addTaskByPrincipal/{id}", method = RequestMethod.GET)
    public String addTaskByPrincipal(ModelMap model, @PathVariable Long id) {
        model.addAttribute("user_id", id);
        return "/task/AddTaskByPrincipal";
    }

    @RequestMapping(value = "/addTaskByPrincipal/{id}", method = RequestMethod.POST)
    public String addTaskPostByPrincipal(ModelMap model, @PathVariable Long id) {
        model.addAttribute("user_id", id);
        return "/task/AddTaskByPrincipal";
    }

    @RequestMapping(value = "/deleteUser/{id}")
    public String deleteUser(ModelMap model, @PathVariable Long id) {
        model.addAttribute("user_id", id);
        return "/user/DeleteUser";
    }

    @RequestMapping(value = "/editUser/{id}")
    public String editUser(ModelMap model, @PathVariable Long id) {
        model.addAttribute("user_id", id);
        return "/user/EditUser";
    }

    @RequestMapping(value = "/deleteTask/{id}")
    public String deleteTask(ModelMap model, @PathVariable Long id) {
        model.addAttribute("taskId", id);
        return "/task/DeleteTask";
    }

    @RequestMapping(value = "/editTask/{id}")
    public String editTask(ModelMap model, @PathVariable Long id) {
        model.addAttribute("taskId", id);
        return "/task/EditTask";
    }

    @RequestMapping(value = "/userPage/{id}")
    public String getUserPage(ModelMap model, @PathVariable Long id) {
        model.addAttribute("user_id", id);
        return "/user/UserPage";
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(ModelMap model) {
        model.addAttribute("notification", "");
        return "Index";
    }

    @RequestMapping(value = "/index", method = RequestMethod.POST)
    public String tryLogin(ModelMap model, @ModelAttribute AuthorizationData authorizationData) {
        Optional<User> userOptional = userService.getUserByLogin(authorizationData.getLogin());
        if (!userOptional.isPresent()) {
            model.addAttribute("notification", "Invalid login");
            return "Index";
        }
        User user = userOptional.get();
        if (!userService.confirmPassword(user, authorizationData.getPassword())) {
            model.addAttribute("notification", "Invalid password");
            return "Index";
        }
        String keySession = RandomStringUtils.randomAlphanumeric(30);
        model.addAttribute("sessionId", keySession);
        model.addAttribute("userId", user.getUserId());
        return "LoginSuccessful";
    }

    @RequestMapping(value = "/logout")
    public String tryLogout(ModelMap model) {
        return "Logout";
    }
}
