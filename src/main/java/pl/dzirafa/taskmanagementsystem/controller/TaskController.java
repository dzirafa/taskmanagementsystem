package pl.dzirafa.taskmanagementsystem.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.dzirafa.taskmanagementsystem.model.Task;
import pl.dzirafa.taskmanagementsystem.model.User;
import pl.dzirafa.taskmanagementsystem.model.UserNameWithIdPair;
import pl.dzirafa.taskmanagementsystem.model.responses.Response;
import pl.dzirafa.taskmanagementsystem.model.responses.ResponseFactory;
import pl.dzirafa.taskmanagementsystem.service.TaskService;
import pl.dzirafa.taskmanagementsystem.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by dzirafa on 19.12.2017
 */
@RestController
@RequestMapping(value = "/rest/")
public class TaskController {

    @Autowired
    TaskService taskService;
    @Autowired
    UserService userService;

    @RequestMapping(value = "/task/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> getTaskWithId (@PathVariable Long id){
        Optional<Task> task = taskService.getTaskWithId(id);
        if (task.isPresent()){
            return new ResponseEntity<Response>(ResponseFactory.success(task.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("Task with that id does not exist."),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/addTask", method = RequestMethod.POST)
    public ResponseEntity<Response> addTaskPost(@RequestBody Task task){
        Logger.getLogger(getClass()).debug("Requested addTask");
        if (taskService.taskExists(task.getTaskId())){
            return new ResponseEntity<Response>(ResponseFactory.failed("Task exists"), HttpStatus.BAD_REQUEST);
        } else {
            taskService.addTask(task, task.getTaskData());
            return new ResponseEntity<Response>(ResponseFactory.success(task),HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/editTask/{id}", method = RequestMethod.POST)
    public ResponseEntity<Response> editTaskWithId (@PathVariable Long id, @RequestBody Task editedTask){
        Logger.getLogger(getClass()).debug("Requested edit task " + id);
        Optional<Task> currentTask = taskService.getTaskWithId(id);
        if (currentTask.isPresent()){
            currentTask.get().setHeader(editedTask.getHeader());
            currentTask.get().setPerformerId(editedTask.getPerformerId());
            currentTask.get().setPrincipalId(editedTask.getPrincipalId());
            currentTask.get().getTaskData().setDescription(editedTask.getTaskData().getDescription());
            currentTask.get().getTaskData().setPriority(editedTask.getTaskData().getPriority());
            currentTask.get().getTaskData().setDeadline(editedTask.getTaskData().getDeadline());
            taskService.editTask(currentTask.get(), currentTask.get().getTaskData());
            return new ResponseEntity<Response>(ResponseFactory.success(currentTask.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("Task with that id does not exist."),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/deleteTask/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> deleteTaskWithId (@PathVariable Long id){
        Optional<Task> task = taskService.getTaskWithId(id);
        if (task.isPresent()){
            taskService.deleteTask(task.get(),task.get().getTaskData());
            return new ResponseEntity<Response>(ResponseFactory.success(), HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("Task with that id does not exist."),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/setTaskIsDone/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> setTaskIsDone(@PathVariable Long id){
        Logger.getLogger(getClass()).debug("Requested setTaskIsDone");
        Optional<Task> task = taskService.getTaskWithId(id);
        if (task.isPresent()){
            taskService.setTaskIsDone(task.get());
            return new ResponseEntity<Response>(ResponseFactory.success(task.get()),HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(ResponseFactory.failed("Task does not exists"),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/setTaskIsUnfinished/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> setTaskIsUnfinished(@PathVariable Long id){
        Logger.getLogger(getClass()).debug("Requested setTaskIsUnfinished");
        Optional<Task> task = taskService.getTaskWithId(id);
        if (task.isPresent()){
            taskService.setTaskIsUnfinished(task.get());
            return new ResponseEntity<Response>(ResponseFactory.success(task.get()),HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(ResponseFactory.failed("Task does not exists"),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/listTasks", method = RequestMethod.GET)
    public ResponseEntity<List<Task>> requestTaskList(){
        return new ResponseEntity<List<Task>>(taskService.getAllTasks(),HttpStatus.OK);
    }

    @RequestMapping(value = "/listTasksOfPerformer/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Task>> requestTaskListOfPerformer(@PathVariable Long id){
        return new ResponseEntity<List<Task>>(taskService.getTasksWithPerformerId(id),HttpStatus.OK);
    }

    @RequestMapping(value = "/listTasksOfPrincipal/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Task>> requestTaskListOfPrincipal(@PathVariable Long id){
        return new ResponseEntity<List<Task>>(taskService.getTasksWithPrincipalId(id),HttpStatus.OK);
    }

    @RequestMapping(value = "/listDoneTasksOfPerformer/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Task>> requestDoneTaskListOfPerformer(@PathVariable Long id){
        return new ResponseEntity<List<Task>>(taskService.getDoneTasksWithPerformerId(id),HttpStatus.OK);
    }

    @RequestMapping(value = "/listUnfinishedTasksOfPerformer/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Task>> requestUnfinishedTaskListOfPerformer(@PathVariable Long id){
        return new ResponseEntity<List<Task>>(taskService.getUnfinishedTasksWithPerformerId(id),HttpStatus.OK);
    }

    @RequestMapping(value = "/qtyUnfinishedTasksOfPerformer/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> requestQuantityUnfinishedTaskOfPerformer(@PathVariable Long id) {
        return new ResponseEntity<Response>(ResponseFactory.success(taskService.countUnfinishedTask(id)), HttpStatus.OK);
    }

    @RequestMapping(value = "/listOfPerformerId", method = RequestMethod.POST)
    public ResponseEntity<List<UserNameWithIdPair>> requestListOfPerformerId(@RequestBody List<Long> listOfId){
        List<UserNameWithIdPair> list = new ArrayList<>();
        for (int i = 0; i < listOfId.size(); i++) {
            Optional<User> user = userService.getUserById(listOfId.get(i));
            if (user.isPresent()){
                list.add(new UserNameWithIdPair(listOfId.get(i),
                        user.get().getLogin()));
            } else {
                list.add(new UserNameWithIdPair(listOfId.get(i),
                        "deleted User"));
            }
        }
        return new ResponseEntity<List<UserNameWithIdPair>>(list, HttpStatus.OK);
    }

}
