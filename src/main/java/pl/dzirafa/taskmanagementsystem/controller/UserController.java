package pl.dzirafa.taskmanagementsystem.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.dzirafa.taskmanagementsystem.model.QtyOfTasksWithIdPair;
import pl.dzirafa.taskmanagementsystem.model.User;
import pl.dzirafa.taskmanagementsystem.model.UserData;
import pl.dzirafa.taskmanagementsystem.model.responses.Response;
import pl.dzirafa.taskmanagementsystem.model.responses.ResponseFactory;
import pl.dzirafa.taskmanagementsystem.service.TaskService;
import pl.dzirafa.taskmanagementsystem.service.UserService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by dzirafa on 18.12.2017
 */

@RestController
@RequestMapping(value = "/rest/")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    TaskService taskService;

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> getUserWithId (@PathVariable Long id){
        Optional<User> user = userService.getUserById(id);
        if (user.isPresent()){
            return new ResponseEntity<Response>(ResponseFactory.success(user.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User with that id does not exist."),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/userExists/{login}", method = RequestMethod.GET)
    public ResponseEntity<Response> requestUserExists(@PathVariable String login) {
        return new ResponseEntity<Response>(ResponseFactory.success(userService.userExist(login)), HttpStatus.OK);
    }

    @RequestMapping(value = "/addUser",method = RequestMethod.GET)
    public ResponseEntity<Response> addUser(@RequestParam String login,
                                            @RequestParam String password,
                                            @RequestParam String name,
                                            @RequestParam String surname,
                                            @RequestParam String email,
                                            @RequestParam String position,
                                            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateOfBirth){
        Logger.getLogger(getClass()).debug("Requested addUser with params:" +
                " " + login + ":" + password);
        if (userService.userExist(login)){
            return new ResponseEntity<Response>(ResponseFactory.failed("User exists"),HttpStatus.BAD_REQUEST);
        } else {
            userService.addUser(new User(login,password), new UserData(name,
                    surname,email, position, dateOfBirth));
            return new ResponseEntity<Response>(ResponseFactory.success(),HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/addUser",method = RequestMethod.POST)
    public ResponseEntity<Response> addUserPost(@RequestBody User user){
        Logger.getLogger(getClass()).debug("Requested addUser ");
        if (userService.userExist(user.getLogin())){
            return new ResponseEntity<Response>(ResponseFactory.failed("User exists"),HttpStatus.BAD_REQUEST);
        } else {
            userService.addUser(user, user.getUserData());
            return new ResponseEntity<Response>(ResponseFactory.success(user),HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/editUser/{id}", method = RequestMethod.POST)
    public ResponseEntity<Response> editUserWithId (@PathVariable Long id, @RequestBody User editedUser){
        Logger.getLogger(getClass()).debug("Requested edit user " + id);
        Optional<User> currentUser = userService.getUserById(id);
        if (currentUser.isPresent()){
            currentUser.get().setLogin(editedUser.getLogin());
            currentUser.get().setPassword(editedUser.getPassword());
            currentUser.get().getUserData().setName(editedUser.getUserData().getName());
            currentUser.get().getUserData().setSurname(editedUser.getUserData().getSurname());
            currentUser.get().getUserData().setEmail(editedUser.getUserData().getEmail());
            currentUser.get().getUserData().setPosition(editedUser.getUserData().getPosition());
            currentUser.get().getUserData().setDateOfBirth(editedUser.getUserData().getDateOfBirth());
            userService.editUser(currentUser.get(), currentUser.get().getUserData());
            return new ResponseEntity<Response>(ResponseFactory.success(currentUser.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User with that id does not exist."),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/deleteUser/{id}", method = RequestMethod.GET)
    public ResponseEntity<Response> deleteUserWithId (@PathVariable Long id){
        Optional<User> user = userService.getUserById(id);
        if (user.isPresent()){
            userService.deleteUser(user.get(),user.get().getUserData());
            return new ResponseEntity<Response>(ResponseFactory.success(), HttpStatus.OK);
        } else {
            return new ResponseEntity<Response>(
                    ResponseFactory.failed("User with that id does not exist."),HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/listUsers", method = RequestMethod.GET)
    public ResponseEntity<List<User>> requestUserList(){
        return new ResponseEntity<List<User>>(userService.getAllUsers(),HttpStatus.OK);
    }

    @RequestMapping(value = "/listOfQtyOfTasks", method = RequestMethod.POST)
    public ResponseEntity<List<QtyOfTasksWithIdPair>> requestListOfQtyOfTasks(@RequestBody List<Long> listOfId){
        List<QtyOfTasksWithIdPair> list = new ArrayList<>();
        for (int i = 0; i < listOfId.size(); i++) {
            list.add(new QtyOfTasksWithIdPair(listOfId.get(i),
                    taskService.countUnfinishedTask(listOfId.get(i))));
        }
        return new ResponseEntity<List<QtyOfTasksWithIdPair>>(list, HttpStatus.OK);
    }
}
