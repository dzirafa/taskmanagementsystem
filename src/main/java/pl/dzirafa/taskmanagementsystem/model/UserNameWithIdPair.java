package pl.dzirafa.taskmanagementsystem.model;

/**
 * Created by dzirafa on 10.01.2018
 */
public class UserNameWithIdPair {
    private long id;
    private String login;


    public UserNameWithIdPair(long id, String login) {
        this.id = id;
        this.login = login;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
