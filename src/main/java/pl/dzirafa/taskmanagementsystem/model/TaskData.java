package pl.dzirafa.taskmanagementsystem.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Created by dzirafa on 19.12.2017
 */
@Entity
@Table(name = "tms_task_data")
public class TaskData {
    @Id
    @Column
    @GeneratedValue
    private long id;

    @Column
    private String description;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate deadline;

    @Column
    private Integer priority;


    public TaskData() {
        this.id = 0L;
    }

    public TaskData(String description, LocalDate deadline, Integer priority, Long performerId, Long principalId) {
        this.id = 0L;
        this.description = description;
        this.deadline = deadline;
        this.priority = priority;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

}
