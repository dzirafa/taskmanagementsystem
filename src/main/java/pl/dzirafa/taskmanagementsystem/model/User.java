package pl.dzirafa.taskmanagementsystem.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dzirafa on 18.12.2017
 */
@Entity
@Table(name = "tms_user")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
    @Id
    @Column(name = "user_id")
    @GeneratedValue
    private long userId;

    @Column
    private String login;

    @Column
    private String password;

    @OneToOne(cascade = CascadeType.PERSIST)
    private UserData userData;

    @OneToMany(fetch = FetchType.EAGER)
    private List<Task> taskList;

    public User() {
        this.userId = 0L;
        this.taskList = new ArrayList<>();
        this.userData = new UserData();
    }

    public User(String login, String password) {
        this.userId = 0L;
        this.login = login;
        this.password = password;
        this.userData = new UserData();
        this.taskList = new ArrayList<>();
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
