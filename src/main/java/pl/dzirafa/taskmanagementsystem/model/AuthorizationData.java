package pl.dzirafa.taskmanagementsystem.model;

/**
 * Created by dzirafa on 10.01.2018
 */
public class AuthorizationData {
    private String login;
    private String password;
    private boolean remember;

    public AuthorizationData() {
    }

    public AuthorizationData(String login, String password, boolean remember) {
        this.login = login;
        this.password = password;
        this.remember = remember;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRemember() {
        return remember;
    }

    public void setRemember(boolean remember) {
        this.remember = remember;
    }
}
