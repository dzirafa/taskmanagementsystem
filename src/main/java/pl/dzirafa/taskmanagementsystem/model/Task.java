package pl.dzirafa.taskmanagementsystem.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

/**
 * Created by dzirafa on 18.12.2017
 */
@Entity
@Table(name = "tms_task")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task {

    @Id
    @Column(name = "task_id")
    @GeneratedValue
    private Long taskId;

    @Column(name = "task_is_done")
    private boolean isDone;

    @Column
    private String header;

    @Column(name = "task_performer_id")
    private Long performerId;

    @Column(name = "task_principal_id")
    private Long principalId;

    @OneToOne(cascade = CascadeType.PERSIST)
    private TaskData taskData;

    public Task() {
        this.taskId = 0L;
    }

    public Task(boolean isDone, String header, Long performerId, Long principalId) {
        this.taskId = 0L;
        this.isDone = isDone;
        this.header = header;
        this.performerId = performerId;
        this.principalId = principalId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public TaskData getTaskData() {
        return taskData;
    }

    public void setTaskData(TaskData taskData) {
        this.taskData = taskData;
    }

    public Long getPerformerId() {
        return performerId;
    }

    public void setPerformerId(Long performerId) {
        this.performerId = performerId;
    }

    public Long getPrincipalId() {
        return principalId;
    }

    public void setPrincipalId(Long principalId) {
        this.principalId = principalId;
    }

    @Override
    public String toString() {
        return "Task{" +
                "taskId=" + taskId +
                ", isDone=" + isDone +
                ", header='" + header + '\'' +
                '}';
    }
}
