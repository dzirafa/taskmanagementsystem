package pl.dzirafa.taskmanagementsystem.model;

/**
 * Created by dzirafa on 11.01.2018
 */
public class QtyOfTasksWithIdPair {
    private Long id;
    private Integer qty;

    public QtyOfTasksWithIdPair() {
    }

    public QtyOfTasksWithIdPair(Long id, Integer qty) {
        this.id = id;
        this.qty = qty;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }
}
