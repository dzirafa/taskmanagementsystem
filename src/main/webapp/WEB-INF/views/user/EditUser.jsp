<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update User!</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>
</head>
<body ng-app="tmsapp">
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Task management system</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a class="btn btn-outline-success my-2 my-sm-0" href="/logout" role="button">Logout</a>
        </form>
    </div>
</nav>

<div ng-controller="UserController as ctrl">
    <main role="main">

        <div id="sessionDiv" ng-controller="SessionController as session">
            <div data-ng-init="isLoggedIn()">
            </div>
        </div>

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container" data-ng-init="onloadFunction(${user_id})">
                <h1 class="display-3">User <span ng-bind="ctrl.user.login"></span> update form</h1>
                <p>Please fill all fields with correct data.</p>
            </div>
        </div>


        <div class="container" data-ng-init="onloadFunction(${user_id})">
            <form ng-submit="ctrl.submitEditedUser(${user_id})" name="editForm" class="form-horizontal">

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="login">Login</label>
                        <input type="login" ng-model="ctrl.user.login" class="form-control" id="login"
                               value="{{ctrl.user.login}}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="userPassword">Password</label>
                        <input type="password" ng-model="ctrl.user.password" class="form-control" id="userPassword"
                               placeholder="Password">
                    </div>
                </div>


                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="userName">Name</label>
                        <input type="text" ng-model="ctrl.user.userData.name" class="form-control" id="userName"
                               placeholder="Name">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="userSurname">Surname</label>
                        <input type="text" ng-model="ctrl.user.userData.surname" class="form-control" id="userSurname"
                               placeholder="Surname">
                    </div>
                </div>

                <div class="form-group">
                    <label for="userEmail">Email</label>
                    <input type="email" ng-model="ctrl.user.userData.email" class="form-control" id="userEmail"
                           placeholder="Email">
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="userPositon">Position</label>
                        <input type="text" ng-model="ctrl.user.userData.position" class="form-control"
                               id="userPositon" placeholder="Position">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="userDateOfBirth">Date of birth - predefined
                            {{ctrl.user.userData.dateOfBirth}}</label>
                        <input type="date" ng-model="ctrl.user.userData.dateOfBirth" class="form-control"
                               placeholder="yyyy-MM-dd" min="1970-01-01" max="2018-12-31" required
                               id="userDateOfBirth">
                    </div>
                </div>

                <div class="row">
                    <div class="form-actions floatRight">
                        <input type="submit" value="Update" class="btn btn-primary btn-sm"
                               ng-disabled="editForm.$invalid">
                        <button type="submit" ng-click="ctrl.back()" class="btn btn-primary btn-sm">Cancel</button>
                    </div>
                </div>
            </form>
        </div>

    </main>
    <script src="/static/js/app.js"></script>
    <script src="/static/js/services/user_service.js"></script>
    <script src="/static/js/controller/user_controller.js"></script>
    <script src="/static/js/services/task_service.js"></script>
    <script src="/static/js/controller/task_controller.js"></script>
    <script src="/static/js/controller/session_controller.js"></script>
</div>
</body>
</html>
