<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Add new User!</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>

</head>
<body ng-app="tmsapp">
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Task management system</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
</nav>

<main role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">User register form</h1>
            <p>Please fill all fields with correct data.</p>
        </div>
    </div>

    <div ng-controller="UserController as ctrl">

        <div class="container">
            <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                <input type="hidden" ng-model="ctrl.user.userId"/>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="login">Login</label>
                        <input type="login" ng-model="ctrl.user.login" class="form-control" id="login"
                               placeholder="Login">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="userPassword">Password</label>
                        <input type="password" ng-model="ctrl.user.password" class="form-control" id="userPassword"
                               placeholder="Password">
                    </div>
                </div>


                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="userName">Name</label>
                        <input type="text" ng-model="ctrl.user.userData.name" class="form-control" id="userName"
                               placeholder="Name">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="userSurname">Surname</label>
                        <input type="text" ng-model="ctrl.user.userData.surname" class="form-control" id="userSurname"
                               placeholder="Surname">
                    </div>
                </div>

                <div class="form-group">
                    <label for="userEmail">Email</label>
                    <input type="email" ng-model="ctrl.user.userData.email" class="form-control" id="userEmail"
                           placeholder="Email">
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="userPositon">Position</label>
                        <input type="text" ng-model="ctrl.user.userData.position" class="form-control"
                               id="userPositon" placeholder="Position">
                    </div>

                    <div class="form-group col-md-2">
                        <label for="userDateOfBirth">Date of birth</label>
                        <input type="date" ng-model="ctrl.user.userData.dateOfBirth" class="form-control"
                               placeholder="yyyy-MM-dd" min="1970-01-01" max="2018-12-31" required
                               id="userDateOfBirth">
                    </div>
                </div>

                <div class="row">
                    <div class="form-actions floatRight">
                        <input type="submit" value="Add" class="btn btn-primary btn-sm"
                               ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm"
                                ng-disabled="myForm.$pristine">Reset Form
                        </button>
                        <a class="btn btn-primary btn-sm" href="/index" role="button">Back</a>
                    </div>
                </div>
            </form>
        </div>

    </div>

</main>
<script src="/static/js/app.js"></script>
<script src="/static/js/services/user_service.js"></script>
<script src="/static/js/controller/user_controller.js"></script>
<script src="/static/js/services/task_service.js"></script>
<script src="/static/js/controller/task_controller.js"></script>

</body>
</html>
