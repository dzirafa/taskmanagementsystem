<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User page</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>
    <style type="text/css">
        td[data-value="5"] {
            color: red;
            font-weight: 900;
        }

        td[data-value="4"] {
            color: mediumvioletred;
            font-weight: 700;
        }

        td[data-value="3"] {
            color: blueviolet;
            font-weight: 500;
        }

        td[data-value="2"] {
            color: blueviolet;
            font-weight: 300;
        }

        td[data-value="1"] {
            color: blue;
            font-weight: 100;
        }
    </style>
</head>
<body ng-app="tmsapp">
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">

    <a class="navbar-brand" href="#">Task management system</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/userProfile/${user_id}">User profile</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a class="btn btn-outline-success my-2 my-sm-0" href="/logout" role="button">Logout</a>
        </form>
    </div>
</nav>

<div ng-controller="UserController as ctrl">
    <main role="main">
        <div id="sessionDiv" ng-controller="SessionController as session">
            <div data-ng-init="isLoggedIn()">
            </div>
        </div>

        <div class="jumbotron">
            <div class="container" data-ng-init="onloadFunction(${user_id})">
                <h1 class="display-3">Hello, <span ng-bind="ctrl.user.login"></span></h1>
                <p>Today is: <%= java.time.LocalDate.now().toString()%>
                </p>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h2>Your current tasks</h2>
                    <div ng-controller="TaskController as taskCtrl">
                        <div data-ng-init="fetchUnfinishedTaskWithPerformerId(${user_id})">
                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Header</th>
                                    <th scope="col">Deadline</th>
                                    <th scope="col">Priority</th>
                                    <th scope="col">Principal</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Details</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr
                                        ng-repeat="task in taskCtrl.performerUnfinishedTasks">
                                    <th scope="row" ng-bind="task.taskId"></th>
                                    <td ng-bind="task.header"></td>
                                    <td ng-bind="task.taskData.deadline"></td>
                                    <td ng-bind="task.taskData.priority" data-value="{{task.taskData.priority}}"></td>
                                    <td>{{taskCtrl.getTaskPerformer(task.principalId)}}</td>
                                    <td>
                                        <div ng-switch on="task.done">
                                            <span ng-switch-when="true">Completed</span>
                                            <span ng-switch-when="false">Not finished!</span>
                                            <span ng-switch-default>Undefined</span>
                                        </div>
                                    </td>
                                    <td>
                                        <a class="btn btn-warning btn-sm" href="/taskProfile/{{task.taskId}}"
                                           role="button">Details</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <a class="btn btn-outline-primary btn-sm btn-block" href="/addTaskByPrincipal/{{${user_id}}}"
                       role="button">Add new Task</a>
                </div>

                <div class="col-md-5" style="background-color: silver">
                    <h2>Your archival tasks</h2>
                    <div ng-controller="TaskController as taskCtrl">
                        <div data-ng-init="fetchDoneTaskWithPerformerId(${user_id})">
                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Header</th>
                                    <th scope="col">Deadline</th>
                                    <th scope="col">Priority</th>
                                    <th scope="col">Details</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr
                                        ng-repeat="task in taskCtrl.performerDoneTasks">
                                    <th scope="row" ng-bind="task.taskId"></th>
                                    <td ng-bind="task.header"></td>
                                    <td ng-bind="task.taskData.deadline"></td>
                                    <td ng-bind="task.taskData.priority" data-value="{{task.taskData.priority}}"></td>
                                    <td>
                                        <a class="btn btn-dark btn-sm" href="/taskProfile/{{task.taskId}}"
                                           role="button">Details</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <hr/>

            <div class="row">
                <div class="col-md-7" style="background-color: lightblue">
                    <h2>Tasks outsourced to others</h2>
                    <div ng-controller="TaskController as taskCtrl">
                        <div data-ng-init="fetchTaskWithPrincipalId(${user_id})">

                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Header</th>
                                    <th scope="col">Deadline</th>
                                    <th scope="col">Priority</th>
                                    <th scope="col">Performer</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Details</th>
                                </tr>
                                </thead>

                                <tbody>
                                <tr
                                        ng-repeat="task in taskCtrl.principalTasks">
                                    <th scope="row" ng-bind="task.taskId"></th>
                                    <td ng-bind="task.header"></td>
                                    <td ng-bind="task.taskData.deadline"></td>
                                    <td ng-bind="task.taskData.priority" data-value="{{task.taskData.priority}}"></td>
                                    <td>{{taskCtrl.getTaskPerformer(task.performerId)}}</td>
                                    <td>
                                        <div ng-switch on="task.done">
                                            <span ng-switch-when="true"
                                                  style="color: green; font-weight: 400">Completed</span>
                                            <span ng-switch-when="false" style="color: red; font-weight: 600">Not finished!</span>
                                            <span ng-switch-default>Undefined</span>
                                        </div>
                                    </td>
                                    <td>
                                        <a class="btn btn-info btn-sm" href="/taskProfileNotEditable/{{task.taskId}}"
                                           role="button">Details</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="col-md-5" style="background-color: plum">
                    <h2>Users</h2>
                    <div ng-controller="TaskController as taskCtrl">
                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Login</th>
                                <th scope="col">Position</th>
                                <th scope="col">Qty of unfinished tasks</th>
                                <th scope="col">Details</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr ng-repeat="user in ctrl.users">
                                <th scope="row" ng-bind="user.userId"></th>
                                <td ng-bind="user.login"></td>
                                <td ng-bind="user.userData.position"></td>
                                <td style="text-align: center">{{ctrl.getQtyOfPerformerTasks(user.userId)}}</td>
                                <td>
                                    <a class="btn btn-danger btn-sm" href="/userProfileNotEditable/{{user.userId}}"
                                       role="button">Details</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>
    </main>
</div>

<script src="/static/js/app.js"></script>
<script src="/static/js/services/user_service.js"></script>
<script src="/static/js/controller/user_controller.js"></script>
<script src="/static/js/services/task_service.js"></script>
<script src="/static/js/controller/task_controller.js"></script>
<script src="/static/js/controller/session_controller.js"></script>

</body>
</html>
