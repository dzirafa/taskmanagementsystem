<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User profile</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>
</head>
<body ng-app="tmsapp">
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Task management system</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a class="btn btn-outline-success my-2 my-sm-0" href="/logout" role="button">Logout</a>
        </form>
    </div>
</nav>

<div ng-controller="TaskController as taskCtrl">
    <div ng-controller="UserController as ctrl">
        <main role="main">

            <div id="sessionDiv" ng-controller="SessionController as session">
                <div data-ng-init="isLoggedIn()">
                </div>
            </div>

            <div class="jumbotron">
                <div class="container" data-ng-init="onloadFunction(${user_id})">
                    <h1 class="display-3" ng-bind="ctrl.user.login"></h1>
                </div>
            </div>
            <div class="container">
                <table class="table table-sm">

                    <tbody>
                    <tr>
                        <th scope="row">Id:</th>
                        <td ng-bind="ctrl.user.userId">Id</td>
                    </tr>
                    <tr>
                        <th scope="row">Login:</th>
                        <td ng-bind="ctrl.user.login">Login</td>
                    </tr>
                    <tr>
                        <th scope="row">Name:</th>
                        <td ng-bind="ctrl.user.userData.name">Name</td>
                    </tr>
                    <tr>
                        <th scope="row">Surname:</th>
                        <td ng-bind="ctrl.user.userData.surname">Surname</td>
                    </tr>
                    <tr>
                        <th scope="row">E-mail:</th>
                        <td ng-bind="ctrl.user.userData.email">E-mail</td>
                    </tr>
                    <tr>
                        <th scope="row">Position:</th>
                        <td ng-bind="ctrl.user.userData.position">Position</td>
                    </tr>
                    <tr>
                        <th scope="row">Date of birth:</th>
                        <td ng-bind="ctrl.user.userData.dateOfBirth">Date of birth:</td>
                    </tr>
                    <tr>
                        <div data-ng-init="onloadFunction(${user_id})">
                            <th scope="row">Quantity of current tasks:</th>
                            <td ng-bind="ctrl.quantityOfTasks">Quantity</td>
                        </div>
                    </tr>
                    </tbody>
                </table>
                <div class="form-actions floatRight">
                    <a class="btn btn-warning btn-sm" href="/editUser/${user_id}" role="button">Edit user data</a>
                    <a class="btn btn-danger btn-sm" href="/deleteUser/${user_id}" role="button">Delete user</a>
                    <a class="btn btn-primary btn-sm" href="/userPage/${user_id}" role="button">Back</a>
                </div>

            </div>
        </main>
    </div>
</div>

<script src="/static/js/app.js"></script>
<script src="/static/js/services/user_service.js"></script>
<script src="/static/js/controller/user_controller.js"></script>
<script src="/static/js/services/task_service.js"></script>
<script src="/static/js/controller/task_controller.js"></script>
<script src="/static/js/controller/session_controller.js"></script>
</body>
</html>
