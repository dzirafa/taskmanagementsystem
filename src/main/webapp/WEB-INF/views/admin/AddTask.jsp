<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Add new Task!</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
</head>
<body ng-app="tmsapp">
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>

<main role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Add new Task</h1>
            <p>Please fill all fields with correct data.</p>
        </div>
    </div>

    <div ng-controller="UserController as userCtrl">
        <div ng-controller="TaskController as ctrl">

            <div class="container">
                <form ng-submit="ctrl.submit()" name="myFormTask" class="form-horizontal">
                    <input type="hidden" ng-model="ctrl.task.taskId"/>
                    <input type="hidden" ng-model="ctrl.task.isDone" value="false"/>

                    <div class="form-group">
                        <label for="taskHeader">Header</label>
                        <input type="text" ng-model="ctrl.task.header" class="form-control" id="taskHeader"
                               placeholder="Header">
                    </div>

                    <div class="form-group">
                        <label for="taskDescription">Description</label>
                        <textarea ng-model="ctrl.task.taskData.description" class="form-control"
                                  id="taskDescription" rows="3"></textarea>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="taskPerformer">Performer</label>
                            <input type="text" ng-model="ctrl.task.performerId" class="form-control"
                                   id="taskPerformer"
                                   placeholder="Performer">

                        </div>
                        <div class="form-group col-md-6">
                            <label for="taskPrincipal">Principal</label>
                            <input type="text" ng-model="ctrl.task.principalId" class="form-control"
                                   id="taskPrincipal"
                                   placeholder="Principal">
                        </div>
                    </div>


                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="taskPriority">Priority</label>
                            <input type="text" ng-model="ctrl.task.taskData.priority" class="form-control"
                                   id="taskPriority" placeholder="Priority">
                        </div>

                        <div class="form-group col-md-2">
                            <label for="taskDeadline">Deadline</label>
                            <input type="date" ng-model="ctrl.task.taskData.deadline" class="form-control"
                                   id="taskDeadline">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-actions floatRight">
                            <input type="submit" value="Add" class="btn btn-primary btn-sm"
                                   ng-disabled="myFormTask.$invalid">
                            <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm"
                                    ng-disabled="myFormTask.$pristine">Reset Form
                            </button>
                            <button type="button" ng-click="ctrl.back()" class="btn btn-primary btn-sm"
                            >Back
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="/static/js/app.js"></script>
    <script src="/static/js/services/task_service.js"></script>
    <script src="/static/js/controller/task_controller.js"></script>
    <script src="/static/js/services/user_service.js"></script>
    <script src="/static/js/controller/user_controller.js"></script>

</main>
</body>
</html>

