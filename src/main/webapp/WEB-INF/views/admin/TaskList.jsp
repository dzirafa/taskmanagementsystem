<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>TaskList</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="style/css" href="../../static/css/styles.css">

    <style type="text/css">
        td[data-value="5"] {
            color: red;
            font-weight: bold;
        }

        td[data-value="4"] {
            color: mediumvioletred;
        }

        td[data-value="3"] {
            color: blueviolet;
        }

        td[data-value="2"] {
            color: blueviolet;
        }

        td[data-value="1"] {
            color: blue;
        }

    </style>
</head>
<body ng-app="tmsapp">
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Task management system</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
</nav>

<main role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">List of tasks</h1>
        </div>
    </div>

    <div class="container">

        <div class="row">
            <div class="col-md-12">

                <div ng-controller="UserController as userCtrl">
                    <div ng-controller="TaskController as ctrl">
                        <table class="table table-sm">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Header</th>
                                <th scope="col">Deadline</th>
                                <th scope="col">Priority</th>
                                <th scope="col">Performer</th>
                                <th scope="col">Principal</th>
                                <th scope="col">Status</th>
                            </tr>
                            </thead>

                            <tbody>
                            <tr ng-repeat="task in ctrl.tasks">
                                <th scope="row" ng-bind="task.taskId"></th>
                                <td ng-bind="task.header"></td>
                                <td ng-bind="task.taskData.deadline"></td>
                                <td ng-bind="task.taskData.priority" data-value="{{task.taskData.priority}}"></td>
                                <td>{{ctrl.getTaskPerformer(task.performerId)}}</td>
                                <td>{{ctrl.getTaskPerformer(task.principalId)}}</td>
                                <td>
                                    <div ng-switch on="task.done">
                                        <span ng-switch-when="true">Completed</span>
                                        <span ng-switch-when="false">Not finished!</span>
                                        <span ng-switch-default>Undefined</span>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
        <script src="/static/js/app.js"></script>
        <script src="/static/js/services/task_service.js"></script>
        <script src="/static/js/controller/task_controller.js"></script>
        <script src="/static/js/services/user_service.js"></script>
        <script src="/static/js/controller/user_controller.js"></script>
    </div>
</main>
</body>
</html>

