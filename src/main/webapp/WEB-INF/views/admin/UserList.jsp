<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>UserList</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>

</head>
<body ng-app="tmsapp">
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Task management system</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>


</nav>

<main role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">List of users</h1>
        </div>
    </div>

    <div class="container">

        <div class="row">
            <div class="col-md-8">

                <div ng-controller="UserController as ctrl">
                    <table class="table table-sm">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Login</th>
                            <th scope="col">Name</th>
                            <th scope="col">Surname</th>
                            <th scope="col">Position</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr ng-repeat="user in ctrl.users">
                            <th scope="row" ng-bind="user.userId"></th>
                            <td ng-bind="user.login"></td>
                            <td ng-bind="user.userData.name"></td>
                            <td ng-bind="user.userData.surname"></td>
                            <td ng-bind="user.userData.position"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>
</main>
<script src="/static/js/app.js"></script>
<script src="/static/js/services/user_service.js"></script>
<script src="/static/js/controller/user_controller.js"></script>
<script src="/static/js/services/task_service.js"></script>
<script src="/static/js/controller/task_controller.js"></script>

</body>
</html>
