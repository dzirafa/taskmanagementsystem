<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Task profile</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>
    <style type="text/css">
        td[data-value="5"] {
            color: red;
            font-weight: 900;
        }

        td[data-value="4"] {
            color: mediumvioletred;
            font-weight: 700;
        }

        td[data-value="3"] {
            color: blueviolet;
            font-weight: 500;
        }

        td[data-value="2"] {
            color: blueviolet;
            font-weight: 300;
        }

        td[data-value="1"] {
            color: blue;
            font-weight: 100;
        }
    </style>
</head>
<body ng-app="tmsapp">
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Task management system</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a class="btn btn-outline-success my-2 my-sm-0" href="/logout" role="button">Logout</a>
        </form>
    </div>
</nav>

<div ng-controller="UserController as userCtrl">
    <div ng-controller="TaskController as ctrl">
        <main role="main">

            <div id="sessionDiv" ng-controller="SessionController as session">
                <div data-ng-init="isLoggedIn()">
                </div>
            </div>

            <div class="jumbotron">
                <div class="container" data-ng-init="onloadFunction(${taskId})">
                    <h1 class="display-3" ng-bind="ctrl.task.header"></h1>
                </div>
            </div>
            <div class="container">
                <table class="table table-sm">

                    <tbody>
                    <tr>
                        <th scope="row">Id:</th>
                        <td ng-bind="ctrl.task.taskId">Id</td>
                    </tr>
                    <tr>
                        <th scope="row">Header:</th>
                        <td ng-bind="ctrl.task.header">Header</td>
                    </tr>
                    <tr>
                        <th scope="row">Description:</th>
                        <td ng-bind="ctrl.task.taskData.description">Description</td>
                    </tr>
                    <tr>
                        <th scope="row">Deadline:</th>
                        <td ng-bind="ctrl.task.taskData.deadline">Deadline</td>
                    </tr>
                    <tr>
                        <th scope="row">Priority:</th>
                        <td ng-bind="ctrl.task.taskData.priority" data-value="{{ctrl.task.taskData.priority}}">
                            Priority
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Performer:</th>
                        <td>{{ctrl.getTaskPerformer(ctrl.task.performerId)}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Principal:</th>
                        <td>{{ctrl.getTaskPerformer(ctrl.task.principalId)}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Status</th>
                        <td>
                            <div ng-switch on="ctrl.task.done">
                                <span ng-switch-when="true">Completed</span>
                                <span ng-switch-when="false">Not finished!</span>
                                <span ng-switch-default>Undefined</span>
                            </div>

                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="form-actions floatRight">
                    <button type="button" ng-click="ctrl.back()" class="btn btn-primary btn-sm"
                            ng-disabled="myForm.$pristine">Back
                    </button>
                </div>

            </div>
        </main>
    </div>
</div>

<script src="/static/js/app.js"></script>
<script src="/static/js/services/task_service.js"></script>
<script src="/static/js/controller/task_controller.js"></script>
<script src="/static/js/services/user_service.js"></script>
<script src="/static/js/controller/user_controller.js"></script>
<script src="/static/js/controller/session_controller.js"></script>

</body>
</html>

