<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Delete user</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>
</head>
<body ng-app="tmsapp">
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Task management system</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a class="btn btn-outline-success my-2 my-sm-0" href="/logout" role="button">Logout</a>
        </form>
    </div>
</nav>


<div ng-controller="TaskController as ctrl">
    <main role="main">

        <div id="sessionDiv" ng-controller="SessionController as session">
            <div data-ng-init="isLoggedIn()">
            </div>
        </div>

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container" data-ng-init="onloadFunction(${taskId})">
                <h1 class=" display-3">Delete task <span ng-bind="ctrl.task.header"></span></h1>
                <p>Do you really want to delete the Task?</p>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="form-actions floatRight">
                    <button type="button" ng-click="ctrl.deleteTask('${taskId}')" class="btn btn-danger">Confirm
                    </button>
                    <button type="submit" ng-click="ctrl.back()" class="btn btn-primary btn-sm">Cancel</button>
                </div>
            </div>

        </div>
    </main>
</div>


<script src="/static/js/app.js"></script>
<script src="/static/js/services/task_service.js"></script>
<script src="/static/js/controller/task_controller.js"></script>
<script src="/static/js/services/user_service.js"></script>
<script src="/static/js/controller/user_controller.js"></script>
<script src="/static/js/controller/session_controller.js"></script>
</body>
</html>
