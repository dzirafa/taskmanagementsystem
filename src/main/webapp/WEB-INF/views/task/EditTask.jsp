<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Task!</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>
</head>
<body ng-app="tmsapp">
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Task management system</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a class="btn btn-outline-success my-2 my-sm-0" href="/logout" role="button">Logout</a>
        </form>
    </div>
</nav>

<div ng-controller="UserController as userCtrl">
    <div ng-controller="TaskController as ctrl">
        <main role="main">

            <div id="sessionDiv" ng-controller="SessionController as session">
                <div data-ng-init="isLoggedIn()">
                </div>
            </div>

            <!-- Main jumbotron for a primary marketing message or call to action -->
            <div class="jumbotron" ng-controller="TaskController as ctrl">
                <div class="container" data-ng-init="onloadFunction(${taskId})">
                    <h1 class="display-3">Task <span ng-bind="ctrl.task.header"></span> update form</h1>
                    <p>Please fill all fields with correct data.</p>
                </div>
            </div>


            <div class="container" data-ng-init="onloadFunction(${taskId})">
                <form ng-submit="ctrl.submitEditedTask('${taskId}')" name="myFormEditTask" class="form-horizontal">

                    <div class="form-group">
                        <label for="taskHeader">Header</label>
                        <input type="text" ng-model="ctrl.task.header" class="form-control" id="taskHeader"
                               value="{{ctrl.task.header}}">
                    </div>

                    <div class="form-group">
                        <label for="taskDescription">Description</label>
                        <textarea ng-model="ctrl.task.taskData.description" class="form-control"
                                  id="taskDescription" rows="3"></textarea>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="taskPriority">Priority</label>
                            <select class="form-control" id="taskPriority" ng-model="ctrl.task.taskData.priority"
                                    ng-change="myForm.$setPristine()">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option selected>{{ctrl.task.taskData.priority}}</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="taskDeadline">Deadline - predefined {{ctrl.task.taskData.deadline}}</label>
                            <input type="date" ng-model="ctrl.task.taskData.deadline" class="form-control"
                                   id="taskDeadline">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-actions floatRight">
                            <input type="submit" value="Update" class="btn btn-primary btn-sm"
                                   ng-disabled="myFormEditTask.$invalid">
                            <button type="button" ng-click="ctrl.back()" class="btn btn-primary btn-sm">Back
                            </button>
                        </div>
                    </div>
                </form>
            </div>


        </main>
        <script src="/static/js/app.js"></script>
        <script src="/static/js/services/task_service.js"></script>
        <script src="/static/js/controller/task_controller.js"></script>
        <script src="/static/js/services/user_service.js"></script>
        <script src="/static/js/controller/user_controller.js"></script>
        <script src="/static/js/controller/session_controller.js"></script>

    </div>
</div>
</body>
</html>



