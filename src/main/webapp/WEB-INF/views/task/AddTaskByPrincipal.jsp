<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add new Task!</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular-cookies.js"></script>
</head>
<body ng-app="tmsapp">
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Task management system</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a class="btn btn-outline-success my-2 my-sm-0" href="/logout" role="button">Logout</a>
        </form>
    </div>
</nav>

<div ng-controller="UserController as userCtrl">
    <div ng-controller="TaskController as ctrl">
        <main role="main">

            <div id="sessionDiv" ng-controller="SessionController as session">
                <div data-ng-init="isLoggedIn()">
                </div>
            </div>

            <!-- Main jumbotron for a primary marketing message or call to action -->
            <div class="jumbotron" ng-controller="UserController as userCtrl">
                <div class="container" data-ng-init="onloadFunction(${user_id})">
                    <h1 class="display-3">Add new Task by <span ng-bind="userCtrl.user.login"></span></h1>
                    <p>Please fill all fields with correct data.</p>
                </div>
            </div>


            <div class="container">
                <form ng-submit="ctrl.submit('${user_id}')" name="myFormTaskPrincipal" class="form-horizontal">
                    <input type="hidden" ng-model="ctrl.task.taskId"/>
                    <input type="hidden" ng-model="ctrl.task.isDone" value="false"/>

                    <div class="form-group">
                        <label for="taskHeader">Header</label>
                        <input type="text" ng-model="ctrl.task.header" class="form-control" id="taskHeader"
                               placeholder="Header">
                    </div>

                    <div class="form-group">
                        <label for="taskDescription">Description</label>
                        <textarea ng-model="ctrl.task.taskData.description" class="form-control"
                                  id="taskDescription" rows="3"></textarea>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="taskPerformer">Performer</label>
                            <select class="form-control" id="taskPerformer" ng-model="ctrl.task.performerId">
                                <option ng-repeat="user in userCtrl.users" value="{{user.userId}}">{{user.login}}
                                </option>
                            </select>

                        </div>
                        <div ng-controller="UserController as userCtrl">
                            <div class="form-group col-md-6" data-ng-init="onloadFunction(${user_id})">
                                <label for="taskPrincipal">Principal</label>
                                <input type="text" readonly class="form-control-plaintext" id="taskPrincipal"
                                       ng-model="userCtrl.addingPrincipal">
                            </div>
                        </div>
                    </div>


                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="taskPriority">Priority</label>
                            <select class="form-control" id="taskPriority" ng-model="ctrl.task.taskData.priority"
                                    ng-change="myForm.$setPristine()">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>

                        <div class="form-group col-md-2">
                            <label for="taskDeadline">Deadline</label>
                            <input type="date" ng-model="ctrl.task.taskData.deadline" class="form-control"
                                   id="taskDeadline">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-actions floatRight">
                            <input type="submit" value="Add" class="btn btn-primary btn-sm"
                                   ng-disabled="myFormTaskPrincipal.$invalid">
                            <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm"
                                    ng-disabled="myFormTaskPrincipal.$pristine">Reset Form
                            </button>
                            <button type="button" ng-click="ctrl.back()" class="btn btn-primary btn-sm">Back
                            </button>
                        </div>
                    </div>
                </form>
            </div>


        </main>
        <script src="/static/js/app.js"></script>
        <script src="/static/js/services/task_service.js"></script>
        <script src="/static/js/controller/task_controller.js"></script>
        <script src="/static/js/services/user_service.js"></script>
        <script src="/static/js/controller/user_controller.js"></script>
        <script src="/static/js/controller/session_controller.js"></script>


    </div>
</div>
</body>
</html>


