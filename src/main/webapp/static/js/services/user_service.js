'use strict';

angular.module('tmsapp').factory('UserService', ['$http', '$q',
    function ($http, $q) {

        var RESTAPIURL = 'http://localhost:8080/rest/';

        var factory = {
            fetchUserWithId: fetchUserWithId,
            fetchAllUsers: fetchAllUsers,
            addUser: addUser,
            editUser: editUser,
            deleteUser: deleteUser,
            fetchQuantityOfUnfinishedTaskWithPerformerId: fetchQuantityOfUnfinishedTaskWithPerformerId

        };
        console.log('Factory created.');
        return factory;

        function fetchUserWithId(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'user/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchAllUsers() {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'listUsers').then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function addUser(user) {
            var deferred = $q.defer();
            $http.post(RESTAPIURL + 'addUser', user).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    console.log('Error while creating User');
                    deferred.reject(result)
                }
            );
            return deferred.promise;
        }

        function editUser(id, user) {
            var deferred = $q.defer();
            $http.post(RESTAPIURL + 'editUser/' + id, user).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    console.log('Error while editing User');
                    deferred.reject(result)
                }
            );
            return deferred.promise;
        }

        function deleteUser(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'deleteUser/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchQuantityOfUnfinishedTaskWithPerformerId(ids) {
            var deferred = $q.defer();
            $http.post(RESTAPIURL + 'listOfQtyOfTasks', ids).then(
                function (data) {
                    console.log(data);
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

    }]);