'use strict';

angular.module('tmsapp').factory('TaskService', ['$http', '$q',
    function ($http, $q) {

        var RESTAPIURL = 'http://localhost:8080/rest/';

        var factory = {
            fetchTaskWithId: fetchTaskWithId,
            addTask: addTask,
            editTask: editTask,
            deleteTask: deleteTask,
            fetchAllTasks: fetchAllTasks,
            fetchTaskWithPerformerId: fetchTaskWithPerformerId,
            fetchTaskWithPrincipalId: fetchTaskWithPrincipalId,
            fetchDoneTaskWithPerformerId: fetchDoneTaskWithPerformerId,
            fetchUnfinishedTaskWithPerformerId: fetchUnfinishedTaskWithPerformerId,
            setTaskIsDone: setTaskIsDone,
            setTaskIsUnfinished: setTaskIsUnfinished,
            fetchPairs: fetchPairs,
            qtyUnfinishedTasksOfPerformer: qtyUnfinishedTasksOfPerformer
        };
        console.log('Factory created.');
        return factory;

        function fetchTaskWithId(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'task/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function addTask(task) {
            var deferred = $q.defer();
            $http.post(RESTAPIURL + 'addTask', task).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    console.log('Error while creating Task');
                    deferred.reject(result)
                }
            );
            return deferred.promise;
        }

        function editTask(id, task) {
            var deferred = $q.defer();
            $http.post(RESTAPIURL + 'editTask/' + id, task).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    console.log('Error while editing Task');
                    deferred.reject(result)
                }
            );
            return deferred.promise;
        }

        function deleteTask(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'deleteTask/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchAllTasks() {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'listTasks').then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchTaskWithPerformerId(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'listTasksOfPerformer/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchTaskWithPrincipalId(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'listTasksOfPrincipal/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchDoneTaskWithPerformerId(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'listDoneTasksOfPerformer/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchUnfinishedTaskWithPerformerId(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'listUnfinishedTasksOfPerformer/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function setTaskIsDone(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'setTaskIsDone/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function setTaskIsUnfinished(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'setTaskIsUnfinished/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function fetchPairs(ids) {
            var deferred = $q.defer();
            $http.post(RESTAPIURL + 'listOfPerformerId', ids).then(
                function (data) {
                    console.log(data);
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

        function qtyUnfinishedTasksOfPerformer(id) {
            var deferred = $q.defer();
            $http.get(RESTAPIURL + 'qtyUnfinishedTasksOfPerformer/' + id).then(
                function (data) {
                    deferred.resolve(data);
                }, function (result) {
                    deferred.reject(result);
                }
            );
            return deferred.promise;
        }

    }]);