'use strict';

angular.module('tmsapp').controller('TaskController', ['$scope', '$window', '$cookies', 'TaskService',
    function ($scope, $window, $cookies, TaskService) {
        var self = this;

        self.task = {
            taskId: null,
            isDone: false,
            header: '',
            performerId: null,
            principalId: null,
            taskData: {id: null, description: '', deadline: null, priority: null}
        };
        self.taskData = {description: '', deadline: null, priority: null}

        self.tasks = [];
        self.performerTasks = [];
        self.principalTasks = [];
        self.performerDoneTasks = [];
        self.performerUnfinishedTasks = [];
        self.performersNames = [];
        self.principalNames = [];

        console.log('Controller created.');
        self.fetchAllTasksMethod = fetchAllTasks;
        self.fetchTaskWithId = fetchTaskWithId;
        self.addTask = addTask;
        self.editTask = editTask;
        self.deleteTask = deleteTask;
        self.getTaskPerformer = getTaskPerformer;
        self.setTaskIsDone = setTaskIsDone;
        self.setTaskIsUnfinished = setTaskIsUnfinished;
        self.submit = submit;
        self.submitEditedTask = submitEditedTask;
        self.reset = reset;
        self.back = back;
        self.backToUserPage = backToUserPage;
        self.qtyUnfinishedTasksOfPerformer = qtyUnfinishedTasksOfPerformer;

        fetchAllTasks();

        function contains(a, obj) {
            var i = a.length;
            while (i--) {
                if (a[i] === obj) {
                    return true;
                }
            }
            return false;
        }

        function getTaskPerformer(id) {
            for (var i = 0; i < self.performersNames.length; i++) {
                if (self.performersNames[i].id === id) {
                    return self.performersNames[i].login;
                }
            }
            return id;
        }

        function fetchAllTasks() {
            TaskService.fetchAllTasks().then(
                function (response) {
                    console.log(response);
                    self.tasks = response.data;
                    console.log(self.tasks);

                    var performers = [];
                    for (var i = 0; i < self.tasks.length; i++) {
                        if (!contains(performers, self.tasks[i].performerId)) {
                            performers.push(self.tasks[i].performerId);
                        }
                    }
                    console.log('Performers to fetch:' + performers);
                    TaskService.fetchPairs(performers).then(
                        function (resp) {
                            self.performersNames = resp.data;
                        }, function (res) {

                        }
                    );

                    var principals = [];
                    for (var it = 0; it < self.tasks.length; it++) {
                        if (!contains(principals, self.tasks[it].principalId)) {
                            performers.push(self.tasks[it].principalId);
                        }
                    }
                    console.log('Principals to fetch:' + principals);
                    TaskService.fetchPairs(principals).then(
                        function (resp) {
                            self.principalNames = resp.data;
                        }, function (res) {

                        }
                    );
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        function fetchTaskWithId(taskId) {
            console.log('Fetch task with id:' + taskId);
            TaskService.fetchTaskWithId(taskId).then(
                function (response) {
                    console.log(response);
                    var parts = response.data.result.resultObject.taskData.deadline.split('-');
                    var mydate = new Date(parts[0], parts[1]-1, parts[2]);
                    console.log('GMT' + mydate.toDateString());
                    var mydate2 = new Date(mydate.setHours(mydate.getHours()+24));
                    console.log('GMT+02' + mydate2.toDateString());
                    response.data.result.resultObject.taskData.deadline = mydate2.toDateString();
                    self.task = response.data.result.resultObject;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        $scope.fetchTaskWithPerformerId = function (id) {
            console.log('Fetch task with performer id:' + id);
            TaskService.fetchTaskWithPerformerId(id).then(
                function (response) {
                    console.log(response);
                    self.performerTasks = response.data;
                    for (var i=0; i < self.performerTasks.length; i++){
                        var parts = self.performerTasks[i].taskData.deadline.split('-');
                        var mydate = new Date(parts[0], parts[1]-1, parts[2]);
                        console.log('GMT' + mydate.toDateString());
                        var mydate2 = new Date(mydate.setHours(mydate.getHours()+24));
                        console.log('GMT+02' + mydate2.toDateString());
                        self.performerTasks[i].taskData.deadline = mydate2.toDateString();
                    }
                    console.log(response.data);
                },
                function (result) {
                    console.log(result);
                }
            );
        };

        $scope.fetchTaskWithPrincipalId = function (id) {
            console.log('Fetch task with principal id:' + id);
            TaskService.fetchTaskWithPrincipalId(id).then(
                function (response) {
                    console.log(response);
                    self.principalTasks = response.data;
                    for (var i=0; i < self.principalTasks.length; i++){
                        var parts = self.principalTasks[i].taskData.deadline.split('-');
                        var mydate = new Date(parts[0], parts[1]-1, parts[2]);
                        console.log('GMT' + mydate.toDateString());
                        var mydate2 = new Date(mydate.setHours(mydate.getHours()+24));
                        console.log('GMT+02' + mydate2.toDateString());
                        self.principalTasks[i].taskData.deadline = mydate2.toDateString();
                    }
                    console.log(response.data);
                },
                function (result) {
                    console.log(result);
                }
            );
        };

        $scope.fetchDoneTaskWithPerformerId = function (id) {
            console.log('Fetch done task with performer id:' + id);
            TaskService.fetchDoneTaskWithPerformerId(id).then(
                function (response) {
                    console.log(response);
                    self.performerDoneTasks = response.data;
                    for (var i=0; i < self.performerDoneTasks.length; i++){
                        var parts = self.performerDoneTasks[i].taskData.deadline.split('-');
                        var mydate = new Date(parts[0], parts[1]-1, parts[2]);
                        console.log('GMT' + mydate.toDateString());
                        var mydate2 = new Date(mydate.setHours(mydate.getHours()+24));
                        console.log('GMT+02' + mydate2.toDateString());
                        self.performerDoneTasks[i].taskData.deadline = mydate2.toDateString();
                    }
                    console.log(response.data);
                },
                function (result) {
                    console.log(result);
                }
            );
        };

        $scope.fetchUnfinishedTaskWithPerformerId = function (id) {
            console.log('Fetch unfinished task with performer id:' + id);
            TaskService.fetchUnfinishedTaskWithPerformerId(id).then(
                function (response) {
                    console.log(response);
                    self.performerUnfinishedTasks = response.data;
                    for (var i=0; i < self.performerUnfinishedTasks.length; i++){
                        var parts = self.performerUnfinishedTasks[i].taskData.deadline.split('-');
                        var mydate = new Date(parts[0], parts[1]-1, parts[2]);
                        console.log('GMT' + mydate.toDateString());
                        var mydate2 = new Date(mydate.setHours(mydate.getHours()+24));
                        console.log('GMT+02' + mydate2.toDateString());
                        self.performerUnfinishedTasks[i].taskData.deadline = mydate2.toDateString();
                    }
                    console.log(response.data);
                },
                function (result) {
                    console.log(result);
                }
            );
        };

        function setTaskIsDone(taskId) {
            console.log('Setting task as done: ' + taskId);
            TaskService.setTaskIsDone(taskId).then(
                function (successResponse) {
                    console.log(successResponse);
                    self.task = successResponse.data.result.resultObject;
                },
                function (errResponse) {
                    console.error('Error while setting Task is done');
                }
            );
        }

        function setTaskIsUnfinished(taskId) {
            console.log('Setting task as unfinished: ' + taskId);
            TaskService.setTaskIsUnfinished(taskId).then(
                function (successResponse) {
                    console.log(successResponse);
                    self.task = successResponse.data.result.resultObject;
                },
                function (errResponse) {
                    console.error('Error while setting Task is unfinished');
                }
            );
        }

        $scope.onloadFunction = function (id) {
            console.log('ftwid:' + id);
            fetchTaskWithId(id);
        };

        function addTask(task) {
            var convertedData = task.taskData.deadline.toISOString().substring(0, 10);
            task.taskData.deadline = convertedData;
            TaskService.addTask(task).then(
                fetchAllTasks(),
                function (errResponse) {
                    console.error('Error while creating Task');
                }
            )
        }

        function editTask(taskId, task) {
            var convertedData = task.taskData.deadline.toISOString().substring(0, 10);
            task.taskData.deadline = convertedData;
            TaskService.editTask(taskId, task).then(
                fetchAllTasks(),
                function (response) {
                    console.error('Error while updating Task');
                }
            )
        }

        function deleteTask(taskId) {
            console.log('deleting task');
            TaskService.deleteTask(taskId).then(
                fetchAllTasks(),
                $window.location.href = '/userPage/' + self.task.performerId,
                function (errResponse) {
                    console.error('Error while deleting Task');
                }
            )
        }

        function qtyUnfinishedTasksOfPerformer(userId) {
            console.log('Get quantity of unfinished tasks of performer: ' + userId)
            TaskService.qtyUnfinishedTasksOfPerformer(userId);

        }

        function submit(principal_id) {
            self.task.principalId = principal_id;
            if (self.task.taskId === null) {
                console.log('Saving New Task', self.task);
                addTask(self.task);
            } else {
                console.log('Task updated with id ', self.task.taskId);
            }
            reset();
        }

        function submitEditedTask(taskId) {
            console.log('Updating Task', self.task);
            editTask(taskId, self.task);
            $window.location.assign('/userPage/' + self.task.performerId);
            reset();
        }

        function back() {
            $window.location.href = document.referrer;
        }

        function backToUserPage(taskId) {
            fetchTaskWithId(taskId);
            $window.location.href = '/userPage/' + self.task.performerId;
        }

        function reset() {
            self.task = {
                taskId: null,
                isDone: false,
                header: '',
                performerId: null,
                principalId: null,
                taskData: {id: null, description: '', deadline: null, priority: null}
            };
            $scope.myForm.$setPristine();
        }

    }]);