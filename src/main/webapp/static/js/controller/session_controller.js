'use strict';

angular.module('tmsapp').controller('SessionController', ['$scope', '$window', '$cookies',
    function ($scope, $window, $cookies) {
        var self = this;
        self.loggedIn = false;
        self.userId = null;

        console.log('Session controller created.');

        $scope.isLoggedIn = function() {
            var sessionCookieContent = $cookies.get('session_cookie');

            if(sessionCookieContent===undefined){
                $window.location.href = "/index"
            }
        };

        $scope.login = function(sessionId, userId) {
            console.log('Logging in with sessionid = ' + sessionId);
            var a = new Date();
            a = new Date(a.getTime() + 360000);
            console.log('Expiration date:' + a.toGMTString());
            $cookies.put('session_cookie', sessionId, {expires: '' + a.toGMTString()});
            $cookies.put('userId', userId, {expires: '' + a.toGMTString()});
            self.loggedIn = true;
            self.userId = userId;
            $window.location.href= "/userPage/" + userId;
        };

        $scope.logout = function(sessionId) {
            console.log('Logging out with sessionId = ' + sessionId);
            $cookies.remove('session_cookie');
            $cookies.remove('userId');
            self.loggedIn=false;
            $window.location.href = "/index";
        }
    }]);