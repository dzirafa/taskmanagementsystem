'use strict';
angular.module('tmsapp').controller('UserController', ['$scope', '$window', '$cookies', 'UserService', 'TaskService',
    function ($scope, $window, $cookies, UserService, TaskService) {
        var self = this;

        self.user = {
            userId: null,
            login: '',
            password: '',
            userData: {id: null, name: '', surname: '', email: '', position: '', dateOfBirth: null}
        };
        self.userData = {name: '', surname: '', email: '', position: '', dateOfBirth: ''};

        self.users = [];
        self.quantities = [];

        self.addingPrincipal = 0;
        self.quantityOfTasks = 0;

        console.log('Controller created.');
        self.fetchAllUsersMethod = fetchAllUsers;
        self.fetchUserWithId = fetchUserWithId;
        self.addUser = addUser;
        self.editUser = editUser;
        self.deleteUser = deleteUser;
        self.submit = submit;
        self.submitEditedUser = submitEditedUser;
        self.back = back;
        self.reset = reset;
        self.getQtyOfPerformerTasks = getQtyOfPerformerTasks;

        self.addSomethingToSession = addSomethingToSession;
        self.showMeWhatIsInSession = showMeWhatIsInSession;

        fetchAllUsers();

        function contains(a, obj) {
            var i = a.length;
            while (i--) {
                if (a[i] === obj) {
                    return true;
                }
            }
            return false;
        }

        function getQtyOfPerformerTasks(id) {
            for (var i = 0; i < self.quantities.length; i++) {
                if (self.quantities[i].id === id) {
                    return self.quantities[i].qty;
                }
            }
            return 0;
        }

        function showMeWhatIsInSession() {
            var sessionCookieContent = $cookies.get('session_cookie');
            alert('' + sessionCookieContent);
        }

        function addSomethingToSession() {
            var variableWithValue = document.getElementById('sessionValue').value;
            console.log('writing ' + variableWithValue + ' to session');
            var a = new Date();
            a = new Date(a.getTime() + 30000);
            console.log('Expiration date:' + a.toGMTString());
            $cookies.put('session_cookie', variableWithValue, {expires: '' + a.toGMTString()});
        }

        function fetchAllUsers() {
            UserService.fetchAllUsers().then(
                function (response) {
                    console.log(response);
                    self.users = response.data;

                    var performers = [];
                    for (var it = 0; it < self.users.length; it++) {
                        if (!contains(performers, self.users[it].userId)) {
                            performers.push(self.users[it].userId);
                        }
                    }
                    console.log('Performers to fetch:' + performers);
                    UserService.fetchQuantityOfUnfinishedTaskWithPerformerId(performers).then(
                        function (resp) {
                            self.quantities = resp.data;
                        }, function (res) {

                        }
                    );
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        function fetchUserWithId(userId) {
            console.log('Fetch user with id:' + userId);
            UserService.fetchUserWithId(userId).then(
                function (response) {
                    console.log(response);
                    var parts =response.data.result.resultObject.userData.dateOfBirth.split('-');
                    var mydate = new Date(parts[0], parts[1]-1, parts[2]);
                    console.log('GMT' + mydate.toDateString());
                    var mydate2 = new Date(mydate.setHours(mydate.getHours()+24));
                    console.log('GMT+02' + mydate2.toDateString());
                    response.data.result.resultObject.userData.dateOfBirth = mydate2.toDateString();
                    self.user = response.data.result.resultObject;
                },
                function (result) {
                    console.log(result);
                }
            );
        }

        $scope.onloadFunction = function (id) {
            self.addingPrincipal = id;
            TaskService.qtyUnfinishedTasksOfPerformer(id).then(
                function (response) {
                    console.log(response);
                    self.quantityOfTasks = response.data.result.resultObject;
                },
                function (result) {
                    console.log(result);
                }
            );
            console.log('fuwid:' + id);
            fetchUserWithId(id);
        };

        function addUser(user) {
            var convertedData = user.userData.dateOfBirth.toISOString().substring(0, 10);
            user.userData.dateOfBirth = convertedData;
            UserService.addUser(user).then(
                fetchAllUsers(),
                function (errResponse) {
                    console.error('Error while creating User');
                }
            )
        }

        function editUser(userId, user) {
            var convertedData = user.userData.dateOfBirth.toISOString().substring(0, 10);
            user.userData.dateOfBirth = convertedData;
            UserService.editUser(userId, user)
                .then(
                    fetchAllUsers(),
                    function (response) {
                        console.error('Error while updating User');
                    }
                );
        }

        function deleteUser(userId) {
            console.log('deleting');
            UserService.deleteUser(userId).then(
                fetchAllUsers(),
                $window.location.href = '/index',
                function (errResponse) {
                    console.error('Error while deleting User');
                }
            )
        }

        function submit() {
            if (self.user.userId === null) {
                console.log('Saving New User', self.user);
                addUser(self.user);
                $window.history.back();
            } else {
                console.log('User updated with id ', self.user.userId);
            }
            reset();
        }

        function submitEditedUser(userId) {
            console.log('Updating User', self.user);
            editUser(userId, self.user);
            $window.location.href = '/userPage/' + userId;
            reset();
        }

        function back() {
            $window.location.href = document.referrer;
        }

        function reset() {
            self.user = {
                userId: null,
                login: '',
                password: '',
                userData: {id: null, name: '', surname: '', email: '', position: '', dateOfBirth: null}
            };
            $scope.myForm.$setPristine();
        }

    }]);