# Task Management System

This is a web application to manage tasks in a company. It's made for educational purpose by Rafal Dzido.

## Functionality

The app allows to create User's (employee's) account. There is a login form with fields: login, password, name, surname, email, position (in the company) and date of birth.
After typing login and password you get the access to main page of the app – User Page. You can find there the list of current tasks of the user, list of archival tasks, list of outsourced tasks to others and the list of users.
In addition to the user, there is also another being – Task – with fields: header, description, deadline (date), priority (1-5 value), performer's ID, principal's ID and status.
List of current tasks – there are only those tasks of user which are not completed. For each tasks you can see details on separated page after clicking 'Details' button (with an option of editing and deleting the task; you can also set the task is finished or unfinished).
List of archival tasks – there are only those tasks of user which are completed. For each task you can see details on separated page after clicking 'Details' button (with an option of editing and deleting the task; you can also set the task is finished or unfinished).
List of outsourced tasks to others – there are only those tasks of user in which principal's ID leads to the user.  For each task you can see details on separated page after clicking 'Details' button (the task is not editable).
List of users – list of users with information about quantities of unfinished tasks. For each user you can see details on separated page after clicking 'Details' button (the user is not editable).
Below the list of current tasks it's an option to add new task, where the principal's ID is the same as the user's ID.
On the User Page on the top you can find redirection to antother page with details of user profile. There are also editing and deleting the user's account possibility.
On the top right you can find the Logout button.

## Getting Started

### Installing

In order to start the application on your local machine it's needed to load pom.xml file (with all dependencies) on your IDE. You should define your username and password to access PostgreSQL database in application.properties file(PostgreSQL with e.g. pgAdmin is needed to be installed).

## Built With

* Java 1.8 – backend
* JavaServer Pages
* Hibernate
* Spring Framework
* Angular + JavaScript + Bootstrap – frontend
* Maven – dependency management
* PostgreSQL – object-relational database management system
* Apache Tomcat
* REST API

## Ideas for developement

* Adding possibility to send message between two users
* Adding the board with tasks to pick by the users (tasks defined with no performer's ID)
* Adding the page with user's notes
* Adding sorting on lists by different criteria
* Security problem
* Adding roles to users – worker, manager etc.

## Author

* **Rafal Dzido** - www.linkedin.com/in/rafał-dzido/